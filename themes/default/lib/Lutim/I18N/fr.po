# Lutim language file
# Copyright (C) 2014 Luc Didry
# This file is distributed under the same license as the Lutim package.
# 
# Translators:
# Translators:
# Luc Didry <luc@didry.org>, 2015
msgid ""
msgstr ""
"Project-Id-Version: Lutim\n"
"POT-Creation-Date: YEAR-MO-DA HO:MI+ZONE\n"
"PO-Revision-Date: 2017-06-05 15:54+0000\n"
"Last-Translator: Luc Didry <luc@didry.org>\n"
"Language-Team: French (http://www.transifex.com/fiat-tux/lutim/language/fr/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fr\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. (7)
#. (30)
#. ($delay)
#. (config('max_delay')
#: lib/Lutim/Command/cron/stats.pm:149 lib/Lutim/Command/cron/stats.pm:150 lib/Lutim/Command/cron/stats.pm:160 lib/Lutim/Command/cron/stats.pm:161 lib/Lutim/Command/cron/stats.pm:177 lib/Lutim/Command/cron/stats.pm:178 themes/default/templates/partial/for_my_delay.html.ep:12 themes/default/templates/partial/for_my_delay.html.ep:13 themes/default/templates/partial/for_my_delay.html.ep:3 themes/default/templates/partial/lutim.js.ep:138 themes/default/templates/partial/lutim.js.ep:147 themes/default/templates/partial/lutim.js.ep:148 themes/default/templates/raw.html.ep:19 themes/default/templates/raw.html.ep:20 themes/default/templates/raw.html.ep:36 themes/default/templates/raw.html.ep:37 themes/default/templates/raw.html.ep:8 themes/default/templates/raw.html.ep:9
msgid "%1 days"
msgstr "%1 jours"

#. ($total)
#: themes/default/templates/stats.html.ep:2
msgid "%1 sent images on this instance from beginning."
msgstr "%1 images envoyées sur cette instance depuis le début."

#: themes/default/templates/index.html.ep:190
msgid "-or-"
msgstr "-ou-"

#: lib/Lutim.pm:192 lib/Lutim/Command/cron/stats.pm:151 lib/Lutim/Command/cron/stats.pm:162 lib/Lutim/Command/cron/stats.pm:179 themes/default/templates/index.html.ep:5 themes/default/templates/raw.html.ep:10 themes/default/templates/raw.html.ep:21 themes/default/templates/raw.html.ep:38
msgid "1 year"
msgstr "1 an"

#: lib/Lutim.pm:191 lib/Lutim/Command/cron/stats.pm:148 lib/Lutim/Command/cron/stats.pm:159 lib/Lutim/Command/cron/stats.pm:176 themes/default/templates/index.html.ep:4 themes/default/templates/partial/for_my_delay.html.ep:12 themes/default/templates/partial/lutim.js.ep:147 themes/default/templates/raw.html.ep:18 themes/default/templates/raw.html.ep:35 themes/default/templates/raw.html.ep:7
msgid "24 hours"
msgstr "24 heures"

#: themes/default/templates/partial/myfiles.js.ep:57
msgid ": Error while trying to get the counter."
msgstr " : Erreur en essayant de récupérer le compteur."

#: lib/Lutim/Command/cron/stats.pm:144 themes/default/templates/raw.html.ep:3
msgid "Active images"
msgstr "Images actives"

#: lib/Lutim/Controller.pm:288
msgid "An error occured while downloading the image."
msgstr "Une erreur est survenue lors du téléchargement de l’image."

#: themes/default/templates/zip.html.ep:2
msgid "Archives download"
msgstr "Téléchargement d’archives"

#: themes/default/templates/about.html.ep:41 themes/default/templates/myfiles.html.ep:64 themes/default/templates/stats.html.ep:25
msgid "Back to homepage"
msgstr "Retour à la page d’accueil"

#: themes/default/templates/index.html.ep:193 themes/default/templates/index.html.ep:194
msgid "Click to open the file browser"
msgstr "Cliquez pour utiliser le navigateur de fichier"

#: themes/default/templates/about.html.ep:30
msgid "Contributors"
msgstr "Contributeurs"

#: themes/default/templates/partial/lutim.js.ep:214 themes/default/templates/partial/lutim.js.ep:268 themes/default/templates/partial/lutim.js.ep:346
msgid "Copy all view links to clipboard"
msgstr "Copier tous les liens de visualisation dans le presse-papier"

#: themes/default/templates/index.html.ep:18 themes/default/templates/index.html.ep:36 themes/default/templates/index.html.ep:69 themes/default/templates/index.html.ep:77 themes/default/templates/index.html.ep:85 themes/default/templates/index.html.ep:93 themes/default/templates/myfiles.html.ep:20 themes/default/templates/myfiles.html.ep:38 themes/default/templates/partial/common.js.ep:150 themes/default/templates/partial/lutim.js.ep:105 themes/default/templates/partial/lutim.js.ep:120 themes/default/templates/partial/lutim.js.ep:79 themes/default/templates/partial/lutim.js.ep:91
msgid "Copy to clipboard"
msgstr "Copier dans le presse-papier"

#: themes/default/templates/myfiles.html.ep:52
msgid "Counter"
msgstr "Compteur"

#: themes/default/templates/stats.html.ep:18
msgid "Delay repartition chart for disabled images"
msgstr "Graphe de répartition des délais pour les images supprimées"

#: themes/default/templates/stats.html.ep:15
msgid "Delay repartition chart for enabled images"
msgstr "Graphe de répartition des délais pour les images actives"

#: themes/default/templates/index.html.ep:115 themes/default/templates/index.html.ep:147 themes/default/templates/index.html.ep:178 themes/default/templates/myfiles.html.ep:53 themes/default/templates/partial/lutim.js.ep:159
msgid "Delete at first view?"
msgstr "Supprimer au premier accès ?"

#: lib/Lutim/Command/cron/stats.pm:145 themes/default/templates/raw.html.ep:4
msgid "Deleted images"
msgstr "Images supprimées"

#: lib/Lutim/Command/cron/stats.pm:146 themes/default/templates/raw.html.ep:5
msgid "Deleted images in 30 days"
msgstr "Images supprimées dans 30 jours"

#: themes/default/templates/index.html.ep:98 themes/default/templates/myfiles.html.ep:56 themes/default/templates/partial/common.js.ep:142 themes/default/templates/partial/common.js.ep:145
msgid "Deletion link"
msgstr "Lien de suppression"

#: themes/default/templates/gallery.html.ep:6
msgid "Download all images"
msgstr "Télécharger toutes les images"

#: themes/default/templates/index.html.ep:81 themes/default/templates/index.html.ep:83 themes/default/templates/partial/lutim.js.ep:101 themes/default/templates/partial/lutim.js.ep:97
msgid "Download link"
msgstr "Lien de téléchargement"

#: themes/default/templates/index.html.ep:28 themes/default/templates/index.html.ep:31 themes/default/templates/myfiles.html.ep:30 themes/default/templates/myfiles.html.ep:33
msgid "Download zip link"
msgstr "Lien de téléchargement de l’archive des images"

#: themes/default/templates/index.html.ep:189
msgid "Drag & drop images here"
msgstr "Déposez vos images ici"

#: themes/default/templates/about.html.ep:7
msgid "Drag and drop an image in the appropriate area or use the traditional way to send files and Lutim will provide you four URLs. One to view the image, an other to directly download it, one you can use on social networks and a last to delete the image when you want."
msgstr "Faites glisser des images dans la zone prévue à cet effet ou sélectionnez un fichier de façon classique et Lutim vous fournira quatre URLs en retour. Une pour afficher l’image, une autre pour la télécharger directement, une pour l’utiliser sur les réseaux sociaux et une dernière pour supprimer votre image quand vous le souhaitez"

#: themes/default/templates/index.html.ep:150 themes/default/templates/index.html.ep:181
msgid "Encrypt the image (Lutim does not keep the key)."
msgstr "Chiffrer l’image (Lutim ne stocke pas la clé)."

#: themes/default/templates/partial/lutim.js.ep:44
msgid "Error while trying to modify the image."
msgstr "Une erreur est survenue en essayant de modifier l’image."

#: themes/default/templates/stats.html.ep:10
msgid "Evolution of total files"
msgstr "Évolution du nombre total de fichiers"

#: themes/default/templates/myfiles.html.ep:55
msgid "Expires at"
msgstr "Expire le"

#: themes/default/templates/myfiles.html.ep:50
msgid "File name"
msgstr "Nom du fichier"

#: themes/default/templates/about.html.ep:24
msgid "For more details, see the <a href=\"https://framagit.org/luc/lutim\">homepage of the project</a>."
msgstr "Pour plus de détails, consultez la page <a href=\"https://framagit.org/luc/lutim\">Github</a> du projet."

#: themes/default/templates/layouts/default.html.ep:55
msgid "Fork me!"
msgstr "Créez un fork !"

#: themes/default/templates/index.html.ep:10 themes/default/templates/index.html.ep:13 themes/default/templates/myfiles.html.ep:12 themes/default/templates/myfiles.html.ep:15
msgid "Gallery link"
msgstr "Lien vers la galerie"

#: themes/default/templates/partial/common.js.ep:104 themes/default/templates/partial/common.js.ep:87
msgid "Hit Ctrl+C, then Enter to copy the short link"
msgstr "Faites Ctrl+C puis appuyez sur la touche Entrée pour copier le lien"

#: themes/default/templates/layouts/default.html.ep:50
msgid "Homepage"
msgstr "Accueil"

#: themes/default/templates/about.html.ep:20
msgid "How do you pronounce Lutim?"
msgstr "Comment doit-on prononcer Lutim ?"

#: themes/default/templates/about.html.ep:6
msgid "How does it work?"
msgstr "Comment ça marche ?"

#: themes/default/templates/about.html.ep:18
msgid "How to report an image?"
msgstr "Comment peut-on faire pour signaler une image ?"

#: themes/default/templates/about.html.ep:14
msgid "If the files are deleted if you ask it while posting it, their SHA512 footprint are retained."
msgstr "Si les fichiers sont bien supprimés si vous en avez exprimé le choix, leur empreinte SHA512 est toutefois conservée."

#: themes/default/templates/index.html.ep:163 themes/default/templates/index.html.ep:203
msgid "Image URL"
msgstr "URL de l’image"

#: lib/Lutim/Command/cron/stats.pm:143 themes/default/templates/raw.html.ep:2
msgid "Image delay"
msgstr "Durée de rétention de l’image"

#: lib/Lutim/Controller.pm:710
msgid "Image not found."
msgstr "Image non trouvée."

#: themes/default/templates/layouts/default.html.ep:54
msgid "Informations"
msgstr "Informations"

#: themes/default/templates/layouts/default.html.ep:62
msgid "Install webapp"
msgstr "Installer la webapp"

#: themes/default/templates/layouts/default.html.ep:61
msgid "Instance's statistics"
msgstr "Statistiques de l’instance"

#: themes/default/templates/about.html.ep:11
msgid "Is it really anonymous?"
msgstr "C’est vraiment anonyme ?"

#: themes/default/templates/about.html.ep:9
msgid "Is it really free (as in free beer)?"
msgstr "C’est vraiment gratuit ?"

#: themes/default/templates/about.html.ep:21
msgid "Juste like you pronounce the French word <a href=\"https://fr.wikipedia.org/wiki/Lutin\">lutin</a> (/ly.tɛ̃/)."
msgstr "Comme on prononce <a href=\"https://fr.wikipedia.org/wiki/Lutin\">lutin</a> (/ly.tɛ̃/)."

#: themes/default/templates/index.html.ep:153 themes/default/templates/index.html.ep:184
msgid "Keep EXIF tags"
msgstr "Conserver les données EXIF"

#: themes/default/templates/index.html.ep:118 themes/default/templates/index.html.ep:166 themes/default/templates/index.html.ep:206 themes/default/templates/partial/lutim.js.ep:163
msgid "Let's go!"
msgstr "Allons-y !"

#: themes/default/templates/layouts/default.html.ep:58
msgid "Liberapay button"
msgstr "Bouton Liberapay"

#: themes/default/templates/layouts/default.html.ep:53
msgid "License:"
msgstr "Licence :"

#: themes/default/templates/index.html.ep:89 themes/default/templates/index.html.ep:91 themes/default/templates/partial/lutim.js.ep:111 themes/default/templates/partial/lutim.js.ep:115
msgid "Link for share on social networks"
msgstr "Lien pour partager sur les réseaux sociaux"

#: themes/default/templates/zip.html.ep:7
msgid "Lutim can't zip so many images at once, so it splitted your demand in multiple URLs."
msgstr "Lutim ne peut zipper autant d’images à la fois, votre demande a donc été découpée en plusieurs URL."

#: themes/default/templates/about.html.ep:4
msgid "Lutim is a free (as in free beer) and anonymous image hosting service. It's also the name of the free (as in free speech) software which provides this service."
msgstr "Lutim est un service gratuit et anonyme d’hébergement d’images. Il s’agit aussi du nom du logiciel (libre) qui fournit ce service."

#: themes/default/templates/about.html.ep:25
msgid "Main developers"
msgstr "Développeurs de l’application"

#: themes/default/templates/index.html.ep:73 themes/default/templates/index.html.ep:75 themes/default/templates/partial/lutim.js.ep:85 themes/default/templates/partial/lutim.js.ep:88
msgid "Markdown syntax"
msgstr "Syntaxe Markdown"

#: themes/default/templates/layouts/default.html.ep:60 themes/default/templates/myfiles.html.ep:2
msgid "My images"
msgstr "Mes images"

#: themes/default/templates/partial/myfiles.js.ep:19
msgid "No limit"
msgstr "Pas de date d’expiration"

#: themes/default/templates/index.html.ep:165 themes/default/templates/index.html.ep:198
msgid "Only images are allowed"
msgstr "Seules les images sont acceptées"

#: themes/default/templates/myfiles.html.ep:6
msgid "Only the images sent with this browser will be listed here. The informations are stored in localStorage: if you delete your localStorage data, you'll loose this informations."
msgstr "Seules les images envoyées avec ce navigateur seront listées ici. Les informations sont stockées en localStorage : si vous supprimez vos données localStorage, vous perdrez ces informations."

#: themes/default/templates/about.html.ep:16
msgid "Only the uploader! (well, only if he's the only owner of the images' rights before the upload)"
msgstr "Seulement l’envoyeur ! (enfin, seulement s’il possède des droits exclusifs sur les images avant de les envoyer)"

#: themes/default/templates/zip.html.ep:12
msgid "Please click on each URL to download the different zip files."
msgstr "Veuillez cliquer sur chaque URL pour télécharger les différents fichiers zip."

#. (config('contact')
#: themes/default/templates/about.html.ep:19
msgid "Please contact the administrator: %1"
msgstr "Veuillez contacter l’administrateur : %1"

#: themes/default/templates/stats.html.ep:22
msgid "Raw stats"
msgstr "Statistiques brutes"

#: themes/default/templates/index.html.ep:158
msgid "Send an image"
msgstr "Envoyer une image"

#: themes/default/templates/partial/lutim.js.ep:20
msgid "Share it!"
msgstr "Partagez !"

#: themes/default/templates/layouts/default.html.ep:56
msgid "Share on Twitter"
msgstr "Partager sur Twitter"

#: themes/default/templates/index.html.ep:133 themes/default/templates/partial/lutim.js.ep:174
msgid "Something bad happened"
msgstr "Un problème est survenu"

#. ($c->config('contact')
#: lib/Lutim/Controller.pm:717
msgid "Something went wrong when creating the zip file. Try again later or contact the administrator (%1)."
msgstr "Quelque chose s’est mal passé lors de la création de l’archive. Veuillez réessayer plus tard ou contactez l’administrateur (%1)."

#: themes/default/templates/layouts/default.html.ep:58
msgid "Support the author on Liberapay"
msgstr "Supporter l’auteur sur Liberapay"

#: themes/default/templates/layouts/default.html.ep:57
msgid "Support the author on Tipeee"
msgstr "Supporter l’auteur sur Tipeee"

#: themes/default/templates/about.html.ep:13
msgid "The IP address of the image's sender is retained for a delay which depends of the administrator's choice (for the official instance, which is located in France, it's one year)."
msgstr ""
"L’IP de la personne ayant déposé l’image est stockée pendant un délai dépendant de l’administrateur de l’instance (pour l’instance officielle, dont le serveur est en France, c’est un délai\n"
" d’un an)."

#: themes/default/templates/about.html.ep:23
msgid "The Lutim software is a <a href=\"http://en.wikipedia.org/wiki/Free_software\">free software</a>, which allows you to download and install it on you own server. Have a look at the <a href=\"https://www.gnu.org/licenses/agpl-3.0.html\">AGPL</a> to see what you can do."
msgstr "Le logiciel Lutim est un <a href=\"https://fr.wikipedia.org/wiki/Logiciel_libre\">logiciel libre</a>, ce qui vous permet de le télécharger et de l’installer sur votre propre serveur. Jetez un coup d’œil à l’<a href=\"https://www.gnu.org/licenses/agpl-3.0.html\">AGPL</a> pour voir quels sont vos droits"

#: lib/Lutim/Controller.pm:307
msgid "The URL is not valid."
msgstr "L’URL n’est pas valide."

#: themes/default/templates/zip.html.ep:16
msgid "The automatic download process will open a tab in your browser for each link. You need to allow popups for Lutim."
msgstr "Le processus de téléchargement automatique va ouvrir un onglet dans votre navigateur pour chaque lien. Vous devez autoriser les fenêtres popup pour Lutim."

#: lib/Lutim/Controller.pm:120 lib/Lutim/Controller.pm:188
msgid "The delete token is invalid."
msgstr "Le jeton de suppression est invalide."

#. ($upload->filename)
#: lib/Lutim/Controller.pm:445
msgid "The file %1 is not an image."
msgstr "Le fichier %1 n’est pas une image."

#. ($tx->res->max_message_size)
#. ($c->req->max_message_size)
#. (config('max_file_size')
#: lib/Lutim/Controller.pm:271 lib/Lutim/Controller.pm:340 themes/default/templates/partial/lutim.js.ep:240
msgid "The file exceed the size limit (%1)"
msgstr "Le fichier dépasse la limite de taille (%1)"

#: themes/default/templates/stats.html.ep:12
msgid "The graph's datas are not updated in real-time."
msgstr "Les données du graphique ne sont pas mises à jour en temps réél."

#. ($image->filename)
#: lib/Lutim/Controller.pm:190
msgid "The image %1 has already been deleted."
msgstr "L’image %1 a déjà été supprimée."

#. ($image->filename)
#: lib/Lutim/Controller.pm:199 lib/Lutim/Controller.pm:204
msgid "The image %1 has been successfully deleted"
msgstr "L’image %1 a été supprimée avec succès."

#: lib/Lutim/Controller.pm:128
msgid "The image's delay has been successfully modified"
msgstr "Le délai de l’image a été modifié avec succès."

#: themes/default/templates/index.html.ep:45
msgid "The images are encrypted on the server (Lutim does not keep the key)."
msgstr "Les images sont chiffrées sur le serveur (Lutim ne stocke pas la clé)."

#: themes/default/templates/about.html.ep:5
msgid "The images you post on Lutim can be stored indefinitely or be deleted at first view or after a delay selected from those proposed."
msgstr "Les images déposées sur Lutim peuvent être stockées indéfiniment, ou s’effacer dès le premier affichage ou au bout du délai choisi parmi ceux proposés."

#. ($c->config->{contact})
#: lib/Lutim/Controller.pm:442
msgid "There is no more available URL. Retry or contact the administrator. %1"
msgstr "Il n’y a plus d’URL disponible. Veuillez réessayer ou contacter l’administrateur. %1."

#: themes/default/templates/layouts/default.html.ep:57
msgid "Tipeee button"
msgstr "Bouton Tipeee"

#: lib/Lutim/Command/cron/stats.pm:152 themes/default/templates/raw.html.ep:11
msgid "Total"
msgstr "Total"

#: themes/default/templates/index.html.ep:60 themes/default/templates/partial/lutim.js.ep:14
msgid "Tweet it!"
msgstr "Tweetez !"

#. ($short)
#: lib/Lutim/Controller.pm:162 lib/Lutim/Controller.pm:233
msgid "Unable to find the image %1."
msgstr "Impossible de trouver l’image %1."

#: lib/Lutim/Controller.pm:529 lib/Lutim/Controller.pm:574 lib/Lutim/Controller.pm:616 lib/Lutim/Controller.pm:659 lib/Lutim/Controller.pm:671 lib/Lutim/Controller.pm:682 lib/Lutim/Controller.pm:707 lib/Lutim/Plugin/Helpers.pm:57
msgid "Unable to find the image: it has been deleted."
msgstr "Impossible de trouver l’image : elle a été supprimée."

#: lib/Lutim/Controller.pm:105
msgid "Unable to get counter"
msgstr "Impossible de récupérer le compteur"

#: themes/default/templates/about.html.ep:17
msgid "Unlike many image sharing services, you don't give us rights on uploaded images."
msgstr "Au contraire de la majorité des services de partages d’image, vous ne nous cédez aucun droit sur les images envoyées."

#: themes/default/templates/index.html.ep:162 themes/default/templates/index.html.ep:201
msgid "Upload an image with its URL"
msgstr "Déposer une image par son URL"

#: themes/default/templates/myfiles.html.ep:54
msgid "Uploaded at"
msgstr "Envoyé le"

#: themes/default/templates/stats.html.ep:6
msgid "Uploaded files by days"
msgstr "Fichiers envoyés, par jour"

#. ($c->app->config('contact')
#: lib/Lutim/Plugin/Helpers.pm:152
msgid "Uploading is currently disabled, please try later or contact the administrator (%1)."
msgstr "L’envoi d’images est actuellement désactivé, veuillez réessayer plus tard ou contacter l’administrateur (%1)."

#: themes/default/templates/index.html.ep:65 themes/default/templates/index.html.ep:67 themes/default/templates/myfiles.html.ep:51 themes/default/templates/partial/lutim.js.ep:71 themes/default/templates/partial/lutim.js.ep:75
msgid "View link"
msgstr "Lien d’affichage"

#: themes/default/templates/about.html.ep:22
msgid "What about the software which provides the service?"
msgstr "Et à propos du logiciel qui fournit le service ?"

#: themes/default/templates/about.html.ep:3
msgid "What is Lutim?"
msgstr "Qu’est-ce que Lutim ?"

#: themes/default/templates/about.html.ep:15
msgid "Who owns rights on images uploaded on Lutim?"
msgstr "Qui possède des droits sur les images envoyées sur Lutim ?"

#: themes/default/templates/about.html.ep:12
msgid "Yes, it is! On the other side, for legal reasons, your IP address will be stored when you send an image. Don't panic, it is normally the case of all sites on which you send files!"
msgstr "Oui, ça l’est ! Par contre, pour des raisons légales, votre adresse IP sera enregistrée lorsque vous enverrez une image. Ne vous affolez pas, c’est de toute façon normalement le cas de tous les sites sur lesquels vous envoyez des fichiers !"

#: themes/default/templates/about.html.ep:10
msgid "Yes, it is! On the other side, if you want to support the developer, you can do it via <a href=\"https://www.tipeee.com/fiat-tux\">Tipeee</a> or via <a href=\"https://liberapay.com/sky/\">Liberapay</a>."
msgstr "Oui, ça l’est ! Par contre, si vous avez envie de soutenir le développeur, vous pouvez faire un microdon avec <a href=\"https://www.tipeee.com/fiat-tux\">Tipeee</a> ou via <a href=\"https://liberapay.com/sky/\">Liberapay</a>."

#: themes/default/templates/zip.html.ep:6
msgid "You asked to download a zip archive for too much files."
msgstr "Vous avez demandé de télécharger une archive zip pour trop de fichiers."

#: themes/default/templates/about.html.ep:8
msgid "You can, optionally, request that the image(s) posted on Lutim to be deleted at first view (or download) or after the delay selected from those proposed."
msgstr "Vous pouvez, de façon facultative, demander à ce que la ou les images déposées sur Lutim soient supprimées après leur premier affichage (ou téléchargement) ou au bout d’un délai choisi parmi ceux proposés."

#: themes/default/templates/about.html.ep:27
msgid "and on"
msgstr "et sur"

#: themes/default/templates/about.html.ep:27
msgid "core developer"
msgstr "développeur principal"

#: lib/Lutim.pm:190 lib/Lutim/Command/cron/stats.pm:147 lib/Lutim/Command/cron/stats.pm:158 lib/Lutim/Command/cron/stats.pm:175 themes/default/templates/index.html.ep:3 themes/default/templates/raw.html.ep:17 themes/default/templates/raw.html.ep:34 themes/default/templates/raw.html.ep:6
msgid "no time limit"
msgstr "Pas de limitation de durée"

#: themes/default/templates/about.html.ep:38
msgid "occitan translation"
msgstr "traduction occitane"

#: themes/default/templates/about.html.ep:27
msgid "on"
msgstr "sur"

#: themes/default/templates/about.html.ep:36
msgid "spanish translation"
msgstr "traduction espagnole"

#: themes/default/templates/about.html.ep:28
msgid "webapp developer"
msgstr "développeur de la webapp"
